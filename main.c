#include <msp430.h> 
#include <stdint.h>

#pragma vector = TIMER0_A1_VECTOR
__interrupt void mijnTimerA0InterruptRoutine(void)
{
    ADC10CTL0 |= ADC10SC;
}

#pragma vector =  ADC10_VECTOR
__interrupt void mijnADCInterruptRoutine(void)
{
    float static tiendeTemperatuur = 0;
    tiendeTemperatuur = ((ADC10MEM / 1023.0) * 1.5) * 1000;
    setTemp((int)tiendeTemperatuur);
    TA0CTL &= ~TAIFG;
}

/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weer te geven
 * temperatuur aan.
 *
 * int temp: De weer te geven temperatuur.
 * Max is 999, min is -99.
 * Hierbuiten wordt "Err-" weergegeven.
 *
 * Voorbeeld:
 * setTemp(100); // geef 10.0 graden weer
 */
void setTemp(int temp);

/* Deze functie geeft bovenaan in het
 * display een titel weer.
 * char tekst[]: de weer te geven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); // geef Hallo weer
 */
void setTitle(char tekst[]);

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer

    // Stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;

    ADC10CTL1 = ADC10DIV_7 | SHS_0 | INCH_5;
    // ADC10DIV_7 = de divider van de klok
    //SHS_0 = selecteeren van ADC10SC
    //INCH_5 = input channel select

    ADC10CTL0 = SREF_1 | ADC10SHT_3 | ADC10SR | REFBURST | REFON | ADC10IE| ADC10ON |  ENC;
    //SREF_1 =
    //ADC10SHT_3 = 64 x ADC10CLKs
    //ADC10SR = sampling rate = ~50 ksps
    //REFBURST = Reference buffer on only during sample-and-conversion
    //REFON =  zet referentiegenerator aan
    //ADC10IE = interupt enable
    //ENC = enables ADC10

    BCSCTL3 = LFXT1S_2; // zet klok op 12 kHz
    TA0CTL = TASSEL_1 | ID_3 | MC_1 | TAIE;
    // TASSEL_1 = Timer A clock source select : 1 - ACLK
    // ID_3 = Timer A input divider : 3 - /8
    // MC_1 = Timer A mode control : 1 - Up mode
    // TAIE = Timer A interupt enable: 1 - aan
    TA0CCR0 = 749;
    // zet tot waar te tellen

    initDisplay();
    __enable_interrupt();
    setTitle("Opdracht7.1.13");

    uint8_t temperatuur = 0; // Fictieve temperatuur

    while (1)
    {
        ADC10CTL0 |= ADC10SC;
        // Hoog fictieve temperatuur op met 0.1 graad
        // Geef dit weer op het display
        //setTemp(temperatuur++);
        // Niet te snel...
    }
}
